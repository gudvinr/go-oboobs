package oboobs

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strings"
)

// Count contains single value
type count struct {
	Count uint `json:"count"`
}

// Response is an object with API response
type Response struct {
	btype    BType
	preview  string
	fullsize string

	Author string `json:"author"`
	ID     int64  `json:"id"`
	Model  string `json:"model"`
	Rank   int64  `json:"rank"`
}

const previewReplace = "_preview"

// Fullsize returns bigger version of image
func (r *Response) Fullsize() string {
	return fmt.Sprintf(mediaEndpoint, r.btype.String()) + r.fullsize
}

// Preview returns link on preview image
func (r *Response) Preview() string {
	return fmt.Sprintf(mediaEndpoint, r.btype.String()) + r.preview
}

// UnmarshalJSON reads preview field since it's unexported and also saves link to fullsize image
func (r *Response) UnmarshalJSON(b []byte) error {
	type Alias Response
	tmp := &struct {
		*Alias
		Preview string
	}{
		Alias: (*Alias)(r),
	}

	if err := json.Unmarshal(b, tmp); err != nil {
		return err
	}

	r.preview = tmp.Preview
	r.fullsize = strings.Replace(r.preview, previewReplace, "", 1)

	if strings.HasPrefix(r.preview, TButts.String()) {
		r.btype = TButts
	}

	return nil
}

func decodeResponse(resp interface{}, url string) error {
	r, err := http.Get(url)
	if err != nil {
		return errors.New("noise: request: " + err.Error())
	}
	defer r.Body.Close()

	err = json.NewDecoder(r.Body).Decode(resp)
	if err != nil {
		return fmt.Errorf("decode: " + err.Error())
	}
	return nil
}
