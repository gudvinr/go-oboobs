# Installation #

`go get -u bitbucket.org/gudvinr/go-oboobs`

# Usage #

``` go
package main

import (
    "fmt"
    "github.com/k0kubun/pp"  // for pretty-printing
    oboobs "bitbucket.org/gudvinr/go-oboobs"
)

func main() {
    btype := oboobs.TBoobs  // TButts for butt pics respectively

    // Query random image
    rand, err := oboobs.Random(btype)  // Get random object
    if err != nil {
        panic(err)
    }
    fmt.Println(rand.Fullsize())  // Prints link to image

    // Get random "noise" image (just previews but more in terms of quantity)
    noisly, err := oboobs.Noise(btype, 1)
    if err != nil {
        panic(err)
    }
    fmt.Println(noisly[0].Preview())  // Prints link to preview image

    // Query a bunch of images
    start := 0
    count := 2
    descending := false

    array, err := oboobs.Query(btype, start, count, oboobs.OrdID, descending)  // Get `count` images with `start` offset ordered by id in ascending order
    if err != nil {
        panic(err)
    }
    pp.Printf("%+v\n", array)  // Prints array of objects

    // Total number of images
    cnt, err := oboobs.Count(btype, false)  // Set second argument to `true` if want to get noise count
    if err != nil {
        panic(err)
    }
    fmt.Printf("count: %d\n", cnt)  // Prints count
}
```