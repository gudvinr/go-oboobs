package oboobs

import (
	"fmt"
	"testing"
)

func runQueryTest(btype BType, t *testing.T) {
	start := 0
	count := 1

	for _, dir := range []bool{true, false} {
		for name, ord := range _OrdTypeNameToValue_map {
			_, err := Query(btype, start, count, OrdType(ord+1), dir)

			prefix := fmt.Sprintf("<order: %s; desc: %t> ", name, dir)

			if err != nil {
				t.Errorf(prefix + err.Error())
			}
		}
	}
}

func runNoiseTest(btype BType, t *testing.T) {
	count := 1

	_, err := Noise(btype, count)

	prefix := fmt.Sprintf("<count: %d> ", count)

	if err != nil {
		t.Errorf(prefix + err.Error())
	}
}

func TestOboobsQuery(t *testing.T) {
	runQueryTest(TBoobs, t)
}

func TestOboobsNoise(t *testing.T) {
	runNoiseTest(TBoobs, t)
}

func TestObuttsQuery(t *testing.T) {
	runQueryTest(TButts, t)
}

func TestObuttsNoise(t *testing.T) {
	runNoiseTest(TButts, t)
}
