package oboobs

import "fmt"

const apiEndpoint = "http://api.o%[1]s.ru/"
const mediaEndpoint = "http://media.o%[1]s.ru/"

const noisePrefix = "noise/"

// BType is just an alias to int
type BType uint8

// OrdType is just an alias to int
type OrdType uint8

//go:generate enumer -type BType -transform=snake -trimprefix=T
const (
	TBoobs BType = iota
	TButts
)

//go:generate enumer -type OrdType -transform=snake -trimprefix=Ord
const (
	OrdRandom OrdType = iota
	OrdID
	OrdRank
	OrdInterest
)

// Count returns the number of images in database
func Count(btype BType, isnoise bool) (uint, error) {
	if btype > TButts || btype < TBoobs {
		return 0, fmt.Errorf("bcount: wrong bodypart type")
	}

	var apiURL = apiEndpoint
	if isnoise {
		apiURL += noisePrefix
	} else {
		apiURL += "%[1]s/"
	}
	apiURL = fmt.Sprintf(apiURL, btype.String()) + "count"

	var data []count
	err := decodeResponse(&data, apiURL)

	return data[0].Count, err
}

// Query returns array of Response objects for given query
// btype indicates whether it boobs (TBoobs) or butts (TButts)
func Query(btype BType, start, count int, order OrdType, desc bool) ([]Response, error) {
	if btype > TButts || btype < TBoobs {
		return nil, fmt.Errorf("query: wrong bodypart type")
	}

	if order < OrdRandom || order > OrdInterest {
		return nil, fmt.Errorf("query: wrong order")
	}

	sort := order.String()
	if desc {
		sort = "-" + sort
	}

	apiURL := fmt.Sprintf(apiEndpoint+"%[1]s/%d/%d/%s", btype.String(), start, count, sort)

	var data []Response
	err := decodeResponse(&data, apiURL)

	return data, err
}

// Random returns single Response with random object
func Random(btype BType) (*Response, error) {
	resp := new(Response)

	data, err := Query(btype, 0, 1, OrdRandom, false)
	if err != nil {
		return nil, err
	}

	*resp = data[0]

	return resp, nil
}

// Noise returns `count` of random preview images
func Noise(btype BType, count int) ([]Response, error) {
	if btype > TButts || btype < TBoobs {
		return nil, fmt.Errorf("noise: wrong bodypart type")
	}

	apiURL := fmt.Sprintf(apiEndpoint+"noise/%d", btype.String(), count)

	var data []Response
	err := decodeResponse(&data, apiURL)

	return data, err
}
